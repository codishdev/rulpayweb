

document.addEventListener('DOMContentLoaded', () => {
  const menuBtn = document.getElementById('menubtn')
  const menu = document.getElementById('menu')
  let show = false
  
    menuBtn.addEventListener('click', () => {
      show = !show
      show ? menu.style.display = 'flex' :  menu.style.display = 'none'
    })
})

